#include <iostream>
#include <vector>
#include <math.h>

#include "uni10.hpp"

using namespace std;

// vector [lamA, lamB, gamA, gamB, U_op]
uni10::UniTensor<uni10_double64> contract_theta(uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [lamA, lamB, gamA, gamB]
uni10::UniTensor<uni10_double64> contract_lglgl(uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [lam, gam]
uni10::UniTensor<uni10_double64> contract_lg(uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [gam, lam]
uni10::UniTensor<uni10_double64> contract_gl(uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [lamA, lamB, gamA, gamB, H]
uni10_double64 H_exp(uni10::Network, uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [lamA, lamB, gamA, gamB, H]
uni10_double64 energy(uni10::Network, uni10::Network, vector<uni10::UniTensor<uni10_double64> >);


int main(){

	cout << "Uni10 1D iTEBD..." << endl;

	cout << "Loading Networks..." << endl;
	uni10::Network LGLGL_net("LGLGL.net");
	uni10::Network Theta_net("Theta.net");
	uni10::Network LG_net("LG.net");
	uni10::Network GL_net("GL.net");

	cout << "Initializing 2-site Hamiltonian of 1-D Ising model..." << endl;
	uni10::Bond phys_bdi(uni10::BD_IN, 2);
	uni10::Bond phys_bdo(uni10::BD_OUT, 2);

	uni10::UniTensor<uni10_double64> Sz({phys_bdi, phys_bdo});
	uni10::UniTensor<uni10_double64> Sx({phys_bdi, phys_bdo});
	uni10::UniTensor<uni10_double64> Id({phys_bdi, phys_bdo});
	uni10::UniTensor<uni10_double64> H, U_op;

	Sz.SetElem({1., 0., 0., -1.});
	Sx.SetElem({0., 1., 1., 0.});
	Id.SetElem({1., 0., 0., 1.});

	H = -0.5 * uni10::Otimes(Sz, Sz) + 0.25 * uni10::Otimes(Id, Sx);
	H.SetName("H");
	cout << H << endl;

	cout << "Initializing imaginary time evolution operator..." << endl;
	float dtau = 0.01;
	U_op.Assign(H.bond());
	U_op.PutBlock(uni10::ExpH(-dtau, H.GetBlock() ) );

	cout << "Initializing Lambda and Gamma..." << endl;
	uni10::Bond virt_bdi(uni10::BD_IN, 1);
	uni10::Bond virt_bdo(uni10::BD_OUT, 1);
	vector<uni10::UniTensor<uni10_double64> > lams(2, uni10::UniTensor<uni10_double64>({virt_bdi, virt_bdo}));
	vector<uni10::UniTensor<uni10_double64> > gams(2, uni10::UniTensor<uni10_double64>({virt_bdi, phys_bdi, virt_bdo}));

	lams[0].SetElem({1.});
	lams[1].SetElem({1.});
	gams[0].SetElem({1., 0.});
	gams[1].SetElem({0., 1.});

	int chi_max = 40, cnt_sch = 0, new_dim;
	uni10::UniTensor<uni10_double64> Theta, lamB_inv;
	vector<uni10::Matrix<uni10_double64> > svd(3);
	uni10_double64* sch_vals;

	cout << "Updating MPS... (with chi_max = " << chi_max << " )" << endl;
	for(int i = 0; i < 400; i ++){

		cnt_sch = 0;
		int idx_a = i % 2;
		int idx_b = abs(idx_a - 1);

		Theta = contract_theta(Theta_net, {lams[idx_a], lams[idx_b], gams[idx_a], gams[idx_b], U_op});
		uni10::Svd(Theta.GetBlock(), svd[0], svd[1], svd[2], uni10::INPLACE);

		// truncate and update tensors
		sch_vals = svd[1].GetElem();
		for(int i = 0; i < svd[1].ElemNum(); i ++){
			if(sch_vals[i] > 1e-9)
				cnt_sch ++;
		}

		new_dim = (cnt_sch < chi_max) ? cnt_sch : chi_max;
		uni10::Resize(svd[0], svd[0].row(), new_dim, uni10::INPLACE);
		uni10::Resize(svd[1], new_dim, new_dim, uni10::INPLACE);
		uni10::Resize(svd[2], new_dim, svd[2].col(), uni10::INPLACE);
		svd[1] *= (1 / uni10::Norm(svd[1]));

		gams[idx_a].Assign({Theta.bond()[0], Theta.bond()[1], uni10::Bond(uni10::BD_OUT, new_dim)} );
		lams[idx_a].Assign({uni10::Bond(uni10::BD_IN, new_dim), uni10::Bond(uni10::BD_OUT, new_dim)} );
		gams[idx_b].Assign({uni10::Bond(uni10::BD_IN, new_dim), Theta.bond()[2], Theta.bond()[3]});

		gams[idx_a].PutBlock(svd[0]);
		lams[idx_a].PutBlock(svd[1]);
		gams[idx_b].PutBlock(svd[2]);

		lamB_inv.Assign(lams[idx_b].bond() );
		lamB_inv.PutBlock(uni10::Inverse(lams[idx_b].GetBlock() ) );

		gams[idx_a] = contract_lg(LG_net, {lamB_inv, gams[idx_a]});
		gams[idx_b] = contract_gl(GL_net, {gams[idx_b], lamB_inv});	
	}

	uni10_double64 E = energy(Theta_net, LGLGL_net, {lams[0], lams[1], gams[0], gams[1], H});
	cout << "Energy = " << E << endl;

	return 0;
}

uni10::UniTensor<uni10_double64> contract_theta(uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("lamA", tens[0]);
	net.PutTensor("lamB", tens[1]);
	net.PutTensor("gamA", tens[2]);
	net.PutTensor("gamB", tens[3]);
	net.PutTensor("U", tens[4]);

	uni10::UniTensor<uni10_double64> theta;
	net.Launch(theta);
	return theta;
}

uni10::UniTensor<uni10_double64> contract_lglgl(uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("lamA", tens[0]);
	net.PutTensor("lamB", tens[1]);
	net.PutTensor("gamA", tens[2]);
	net.PutTensor("gamB", tens[3]);

	uni10::UniTensor<uni10_double64> lglgl;
	net.Launch(lglgl);
	return lglgl;
}

uni10::UniTensor<uni10_double64> contract_lg(uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("lam", tens[0]);
	net.PutTensor("gam", tens[1]);

	uni10::UniTensor<uni10_double64> lg;
	net.Launch(lg);
	return lg;
}

uni10::UniTensor<uni10_double64> contract_gl(uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("gam", tens[0]);
	net.PutTensor("lam", tens[1]);

	uni10::UniTensor<uni10_double64> gl;
	net.Launch(gl);
	return gl;
}

uni10_double64 H_exp(uni10::Network Theta_net, uni10::Network LGLGL_net, vector<uni10::UniTensor<uni10_double64> > tens){

	uni10::UniTensor<uni10_double64> bra, ket;
	bra = contract_lglgl(LGLGL_net, tens);
	ket = contract_theta(Theta_net, tens);

	return uni10::Contract(bra, ket)[0];
}

uni10_double64 energy(uni10::Network Theta_net, uni10::Network LGLGL_net, vector<uni10::UniTensor<uni10_double64> > tens){

	uni10::UniTensor<uni10_double64> lglgl = contract_lglgl(LGLGL_net, tens);
	uni10_double64 ham_exp = H_exp(Theta_net, LGLGL_net, tens);

	return ham_exp / pow(uni10::Contract(lglgl, lglgl)[0], 2);
}