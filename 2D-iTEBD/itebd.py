import pyuni10 as uni10
import numpy as np
import scipy.linalg as linalg
import matplotlib.pyplot as plt

def Contract_Horizontal_Theta(net, GamA, GamB, LamA, LamB, U, Vert_Lam):
	"""
	Contract Horizontal Theta lamB-gamA-lamA-gamB-lamB
	                                 |         |
	                                 -----U-----   
	"""
	net.PutTensor("GamA", GamA)
	net.PutTensor("GamB", GamB)
	net.PutTensor("LamUp", Vert_Lam[0])
	net.PutTensor("LamDown", Vert_Lam[1])
	net.PutTensor("LamA", LamA)
	net.PutTensor("LamB", LamB)
	net.PutTensor("U", U)

	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Horizontal_LGLGL(net, GamA, GamB, LamA, LamB, Vert_Lam):
	"""
	Contract Horizontal LGLGL lamB-gamA-lamA-gamB-lamB
	"""
	net.PutTensor("GamA", GamA)
	net.PutTensor("GamB", GamB)
	net.PutTensor("LamUp", Vert_Lam[0])
	net.PutTensor("LamDown", Vert_Lam[1])
	net.PutTensor("LamA", LamA)
	net.PutTensor("LamB", LamB)

	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Vertical_LGLGL(net, GamA, GamB, LamA, LamB, Hori_Lam):
	"""
	Contract Vertical LGLGL lamB-gamA-lamA-gamB-lamB
	"""
	net.PutTensor("GamA", GamA)
	net.PutTensor("GamB", GamB)
	net.PutTensor("LamLeft", Hori_Lam[0])
	net.PutTensor("LamRight", Hori_Lam[1])
	net.PutTensor("LamA", LamA)
	net.PutTensor("LamB", LamB)
	
	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Vertical_Theta(net, GamA, GamB, LamA, LamB, U, Hori_Lam):
	"""
	Contract Vertical Theta lamB-gamA-lamA-gamB-lamB
								   |         |
								   -----U-----
	"""
	net.PutTensor("GamA", GamA)
	net.PutTensor("GamB", GamB)
	net.PutTensor("LamLeft", Hori_Lam[0])
	net.PutTensor("LamRight", Hori_Lam[1])
	net.PutTensor("LamA", LamA)
	net.PutTensor("LamB", LamB)
	net.PutTensor("U", U)
	
	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Horizontal_GamA_Inverse(net, GamA, LamB, LamUp, LamDown):
	"""
	Contract three inverse horizontal lambda to gammaA
	"""
	net.PutTensor("LamB", LamB)
	net.PutTensor("LamUp", LamUp)
	net.PutTensor("LamDown", LamDown)
	net.PutTensor("GamA", GamA)

	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Horizontal_GamB_Inverse(net, GamB, LamB, LamUp, LamDown):
	"""
	Contract three inverse horizontal lambda to gammaB
	"""
	net.PutTensor("LamB", LamB)
	net.PutTensor("LamUp", LamUp)
	net.PutTensor("LamDown", LamDown)
	net.PutTensor("GamB", GamB)

	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Vertical_GamA_Inverse(net, GamA, LamB, LamLeft, LamRight):
	"""
	Contract three inverse vertical lambdas to gammaA
	"""
	net.PutTensor("LamB", LamB)
	net.PutTensor("LamLeft", LamLeft)
	net.PutTensor("LamRight", LamRight)
	net.PutTensor("GamA", GamA)

	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def Contract_Vertical_GamB_Inverse(net, GamB, LamB, LamLeft, LamRight):
	"""
	Contract three inverse vertical lambdas to gammaB
	"""
	net.PutTensor("LamB", LamB)
	net.PutTensor("LamLeft", LamLeft)
	net.PutTensor("LamRight", LamRight)
	net.PutTensor("GamB", GamB)

	Tout = uni10.UniTensorR([])
	net.Launch(Tout)
	return Tout

def energy(Theta_net, LGLGL_net, Gam, Hori_Lam, Vert_Lam, Ham):
	"""
	Compute energy
	"""
	ket = Contract_Horizontal_Theta(Theta_net, Gam[0], Gam[1], Hori_Lam[0], 
		Hori_Lam[1], Ham, Vert_Lam)
	bra = Contract_Horizontal_LGLGL(LGLGL_net, Gam[0], Gam[1], Hori_Lam[0], 
		Hori_Lam[1], Vert_Lam)

	exp_val = uni10.Contract(bra, ket).GetElem()[0]
	return exp_val / (uni10.Contract(bra, bra).GetElem()[0]**2)

## Load net
Horizontal_Theta_net = uni10.Network("horizontal_theta.net")
Vertical_Theta_net = uni10.Network("vertical_theta.net")
Hori_GamA_Inverse_net = uni10.Network("hori_GamA_inv.net")
Hori_GamB_Inverse_net = uni10.Network("hori_GamB_inv.net")
Vert_GamA_Inverse_net = uni10.Network("vert_GamA_inv.net")
Vert_GamB_Inverse_net = uni10.Network("vert_GamB_inv.net")
Hori_LGLGL_net = uni10.Network("hori_LGLGL.net")
Vert_LGLGL_net = uni10.Network("vert_LGLGL.net")

## Define bond dimension
chi = 10

## Construct 2-site Hamiltonian

phys_bdi = uni10.Bond(uni10.BD_IN, 2)
phys_bdo = uni10.Bond(uni10.BD_OUT, 2)

Sz = uni10.UniTensorR([phys_bdi, phys_bdo])
Sx = uni10.UniTensorR([phys_bdi, phys_bdo])
Id = uni10.UniTensorR([phys_bdi, phys_bdo])

Sz.SetElem([1., 0., 0., -1.])
Sx.SetElem([0., 1., 1., 0.])
Id.Identity()

J = -0.5; h = 0.25
Ham = J * uni10.Otimes(Sz, Sz) + h * uni10.Otimes(Id, Sx)

## Construct evolution operator
dt = 0.01
U_op = uni10.UniTensorR(Ham.bond())
U_op.PutBlock(linalg.expm(-dt * Ham.GetBlock()))

## Initialize Gammas and Lambdas
virt_bdi = uni10.Bond(uni10.BD_IN, chi)
virt_bdo = uni10.Bond(uni10.BD_OUT, chi)

Gammas = [uni10.UniTensorR([virt_bdi, virt_bdi, virt_bdo, virt_bdo, phys_bdo]), 
		  uni10.UniTensorR([virt_bdi, virt_bdi, virt_bdo, virt_bdo, phys_bdo])]
Hori_Lambdas = [uni10.UniTensorR([virt_bdi, virt_bdo]), uni10.UniTensorR([virt_bdi, virt_bdo])] # horizontal lambdas
Vert_Lambdas = [uni10.UniTensorR([virt_bdi, virt_bdo]), uni10.UniTensorR([virt_bdi, virt_bdo])] # vertical lambdas

Gammas[0].Randomize()
Gammas[1].Randomize()
Hori_Lambdas[0].Identity()
Hori_Lambdas[1].Identity()
Vert_Lambdas[0].Identity()
Vert_Lambdas[1].Identity()

for step in range(100):
	print("step : ", step)
	for i in range(4):
		## i = 0, 1 -> Update horizontal itebd
		## i = 2, 3 -> Update vertical itebd
		if (i // 2) == 0:
			## horizontal
			ia = i % 2
			ib = ia - 1

			Theta = Contract_Horizontal_Theta(Horizontal_Theta_net, Gammas[ia], Gammas[ib], 
											  Hori_Lambdas[ia], Hori_Lambdas[ib], U_op, Vert_Lambdas)
			
			U, S, V = linalg.svd(Theta.GetBlock())

			S = np.diag(S)
			S /= linalg.norm(S)

			Gammas[ia].Assign([Theta.bond()[0], Theta.bond()[1], uni10.Bond(uni10.BD_IN, chi), 
							   Theta.bond()[2], Theta.bond()[3].dummy_change(uni10.BD_OUT)])
			Hori_Lambdas[ia].Assign([uni10.Bond(uni10.BD_IN, chi), uni10.Bond(uni10.BD_OUT, chi)])
			Gammas[ib].Assign([uni10.Bond(uni10.BD_IN, chi), Theta.bond()[4].dummy_change(uni10.BD_IN), 
							   Theta.bond()[5].dummy_change(uni10.BD_IN), Theta.bond()[6].dummy_change(uni10.BD_IN), Theta.bond()[7]])
			
			uni10.Permute(Gammas[ia], [0, 1, 3, 4, 2], 4)
			uni10.Permute(Gammas[ib], [1, 2, 3, 4, 0], 4)

			Gammas[ia].PutBlock(U[:, :chi])
			Hori_Lambdas[ia].PutBlock(S[:chi, :chi])
			Gammas[ib].PutBlock(V[:chi, :].T)

			uni10.Permute(Gammas[ia], [0, 1, 2, 3, 4], 2)
			uni10.Permute(Gammas[ib], [0, 1, 2, 3, 4], 2)

			LamB_inv = uni10.UniTensorR(Hori_Lambdas[ib].bond())
			LamUp_inv = uni10.UniTensorR(Vert_Lambdas[0].bond())
			LamDown_inv = uni10.UniTensorR(Vert_Lambdas[1].bond())

			LamB_inv.PutBlock(linalg.inv(Hori_Lambdas[ib].GetBlock()))
			LamUp_inv.PutBlock(linalg.inv(Vert_Lambdas[0].GetBlock()))
			LamDown_inv.PutBlock(linalg.inv(Vert_Lambdas[1].GetBlock()))

			Gammas[ia] = Contract_Horizontal_GamA_Inverse(Hori_GamA_Inverse_net, Gammas[ia], 
														  LamB_inv, LamUp_inv, LamDown_inv)
			Gammas[ib] = Contract_Horizontal_GamB_Inverse(Hori_GamB_Inverse_net, Gammas[ib], 
														  LamB_inv, LamUp_inv, LamDown_inv)
		else:
			## vertical
			ia = i % 2
			ib = ia - 1
			
			Theta = Contract_Vertical_Theta(Vertical_Theta_net, Gammas[ia], Gammas[ib], 
											Vert_Lambdas[ia], Vert_Lambdas[ib], U_op, Hori_Lambdas)

			U, S, V = linalg.svd(Theta.GetBlock())
			S = [sch_val for sch_val in S if sch_val > 10e-9]
			S = np.diag(S)
			S /= linalg.norm(S)

			Gammas[ia].Assign([Theta.bond()[0], Theta.bond()[1], Theta.bond()[2], 
							   uni10.Bond(uni10.BD_IN, chi), Theta.bond()[3].dummy_change(uni10.BD_OUT)])
			Vert_Lambdas[ia].Assign([uni10.Bond(uni10.BD_IN, chi), uni10.Bond(uni10.BD_OUT, chi)])
			Gammas[ib].Assign([Theta.bond()[4].dummy_change(uni10.BD_IN), uni10.Bond(uni10.BD_IN, chi), 
							   Theta.bond()[5].dummy_change(uni10.BD_IN), Theta.bond()[6].dummy_change(uni10.BD_IN), Theta.bond()[7]])

			uni10.Permute(Gammas[ia], [0, 1, 2, 4, 3], 4)
			uni10.Permute(Gammas[ib], [0, 2, 3, 4, 1], 4)

			Gammas[ia].PutBlock(U[:, :chi])
			Vert_Lambdas[ia].PutBlock(S[:chi, :chi])
			Gammas[ib].PutBlock(V[:chi, :].T)

			uni10.Permute(Gammas[ia], [0, 1, 2, 3, 4], 2)
			uni10.Permute(Gammas[ib], [0, 1, 2, 3, 4], 2)

			LamB_inv = uni10.UniTensorR(Vert_Lambdas[ib].bond())
			LamLeft_inv = uni10.UniTensorR(Hori_Lambdas[0].bond())
			LamRight_inv = uni10.UniTensorR(Hori_Lambdas[1].bond())

			LamB_inv.PutBlock(linalg.inv(Vert_Lambdas[ib].GetBlock()))
			LamLeft_inv.PutBlock(linalg.inv(Hori_Lambdas[0].GetBlock()))
			LamRight_inv.PutBlock(linalg.inv(Hori_Lambdas[1].GetBlock()))

			Gammas[ia] = Contract_Vertical_GamA_Inverse(Vert_GamA_Inverse_net, Gammas[ia], 
														LamB_inv, LamLeft_inv, LamRight_inv)
			Gammas[ib] = Contract_Vertical_GamB_Inverse(Vert_GamB_Inverse_net, Gammas[ib], 
														LamB_inv, LamLeft_inv, LamRight_inv)

print("energy = {}".format(energy(Horizontal_Theta_net, Hori_LGLGL_net, Gammas, 
	Hori_Lambdas, Vert_Lambdas, Ham)))