#include <iostream>
#include <vector>
#include <math.h>

#include "uni10.hpp"

using namespace std;

// vector [GamA, GamB, HLamA, HLamB, LamUp, LamDown]
uni10::UniTensor<uni10_double64> Contract_Horizontal_LGLGL(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamA, GamB, HLamA, HLamB, U, LamUp, LamDown]
uni10::UniTensor<uni10_double64> Contract_Horizontal_Theta(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamA, GamB, VLamA, VLamB, LamLeft, LamRight]
uni10::UniTensor<uni10_double64> Contract_Vertical_LGLGL(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamA, GamB, VLamA, VLamB, U, LamLeft, LamRight]
uni10::UniTensor<uni10_double64> Contract_Vertical_Theta(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamA, LamB, LamUp, LamDown]
uni10::UniTensor<uni10_double64> Contract_Horizontal_GamA_Inverse(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamB, LamB, LamUp, LamDown]
uni10::UniTensor<uni10_double64> Contract_Horizontal_GamB_Inverse(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamA, LamB, LamLeft, LamRight]
uni10::UniTensor<uni10_double64> Contract_Vertical_GamA_Inverse(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);

// vector [GamB, LamB, LamLeft, LamRight]
uni10::UniTensor<uni10_double64> Contract_Vertical_GamB_Inverse(
	uni10::Network, vector<uni10::UniTensor<uni10_double64> >);


int main(){

	//// Load net ////
	uni10::Network Horizontal_LGLGL_net("hori_LGLGL.net");
	uni10::Network Horizontal_Theta_net("horizontal_theta.net");
	uni10::Network Vertical_LGLGL_net("vert_LGLGL.net");
	uni10::Network Vertical_Theta_net("vertical_theta.net");
	uni10::Network Hori_GamA_Inverse_net("hori_GamA_inv.net");
	uni10::Network Hori_GamB_Inverse_net("hori_GamB_inv.net");
	uni10::Network Vert_GamA_Inverse_net("vert_GamA_inv.net");
	uni10::Network Vert_GamB_Inverse_net("vert_GamB_inv.net");

	//// Define bond dimension ////
	int chi = 10;

	//// Construct 2-site Hamiltonian ////
	uni10::Bond phys_bdi(uni10::BD_IN, 2);
	uni10::Bond phys_bdo(uni10::BD_OUT, 2);

	uni10::UniTensor<uni10_double64> Sz({phys_bdi, phys_bdo});
	uni10::UniTensor<uni10_double64> Sx({phys_bdi, phys_bdo});
	uni10::UniTensor<uni10_double64> Id({phys_bdi, phys_bdo});
	uni10::UniTensor<uni10_double64> Ham, U_op;

	Sz.SetElem({1., 0., 0., -1.});
	Sx.SetElem({0., 1., 1., 0.});
	Id.Identity();

	Ham = -0.5 * uni10::Otimes(Sz, Sz) + 0.25 * uni10::Otimes(Id, Sx);

	float dtau = 0.01;
	U_op.Assign(Ham.bond());
	U_op.PutBlock(uni10::ExpH(-dtau, Ham.GetBlock()));

	//// Initialize Gammas and Lambdas
	uni10::Bond virt_bdi(uni10::BD_IN, chi);
	uni10::Bond virt_bdo(uni10::BD_OUT, chi);

	vector<uni10::UniTensor<uni10_double64> > Gammas(2, 
		uni10::UniTensor<uni10_double64>({virt_bdi, virt_bdi, virt_bdo, virt_bdo, phys_bdo}));
	
	vector<uni10::UniTensor<uni10_double64> > Hori_Lambdas(2, 
		uni10::UniTensor<uni10_double64>({virt_bdi, virt_bdo}));
	vector<uni10::UniTensor<uni10_double64> > Vert_Lambdas(2, 
		uni10::UniTensor<uni10_double64>({virt_bdi, virt_bdo}));

	Gammas[0].Randomize();
	Gammas[1].Randomize();
	Hori_Lambdas[0].Identity();
	Hori_Lambdas[1].Identity();
	Vert_Lambdas[0].Identity();
	Vert_Lambdas[1].Identity();

	uni10::UniTensor<uni10_double64> Theta, LamB_inv, LamUp_inv, LamDown_inv, LamLeft_inv, LamRight_inv;
	vector<uni10::Matrix<uni10_double64> > svd(3);
	uni10_double64* sch_vals;

	for(int step = 0; step < 50; step ++){
		cout << "step : " << step << endl;

		for(int i = 0; i < 4; i ++){
			// i = 0, 1 -> Update horizontal itebd
			// i = 2, 3 -> Update vertical itebd
			if(i / 2 == 0){
				int ia = i % 2;
				int ib = abs(ia - 1);

				Theta = Contract_Horizontal_Theta(Horizontal_Theta_net, 
					{Gammas[ia], Gammas[ib], Hori_Lambdas[ia], Hori_Lambdas[ib], U_op, Vert_Lambdas[0], Vert_Lambdas[1]});
				uni10::Svd(Theta.GetBlock(), svd[0], svd[1], svd[2], uni10::INPLACE);

				uni10::Resize(svd[0], svd[0].row(), chi, uni10::INPLACE);
				uni10::Resize(svd[1], chi, chi, uni10::INPLACE);
				uni10::Resize(svd[2], chi, svd[2].col(), uni10::INPLACE);
				uni10::Transpose(svd[2], uni10::INPLACE);
				svd[1] *= (1 / uni10::Norm(svd[1]));

				Gammas[ia].Assign({Theta.bond()[0], Theta.bond()[1], uni10::Bond(uni10::BD_IN, chi), 
					Theta.bond()[2], Theta.bond()[3].dummy_change(uni10::BD_OUT)});
				Hori_Lambdas[ia].Assign({uni10::Bond(uni10::BD_IN, chi), uni10::Bond(uni10::BD_OUT, chi)});
				Gammas[ib].Assign({uni10::Bond(uni10::BD_IN, chi), Theta.bond()[4].dummy_change(uni10::BD_IN), 
					Theta.bond()[5].dummy_change(uni10::BD_IN), Theta.bond()[6].dummy_change(uni10::BD_IN), Theta.bond()[7]});

				uni10::Permute(Gammas[ia], {0, 1, 3, 4, 2}, 4, uni10::INPLACE);
				uni10::Permute(Gammas[ib], {1, 2, 3, 4, 0}, 4, uni10::INPLACE);

				Gammas[ia].PutBlock(svd[0]);
				Hori_Lambdas[ia].PutBlock(svd[1]);
				Gammas[ib].PutBlock(svd[2]);

				uni10::Permute(Gammas[ia], {0, 1, 2, 3, 4}, 2, uni10::INPLACE);
				uni10::Permute(Gammas[ib], {0, 1, 2, 3, 4}, 2, uni10::INPLACE);

				LamB_inv.Assign(Hori_Lambdas[ib].bond());
				LamUp_inv.Assign(Vert_Lambdas[0].bond());
				LamDown_inv.Assign(Vert_Lambdas[1].bond());

				LamB_inv.PutBlock(uni10::Inverse(Hori_Lambdas[ib].GetBlock()));
				LamUp_inv.PutBlock(uni10::Inverse(Vert_Lambdas[0].GetBlock()));
				LamDown_inv.PutBlock(uni10::Inverse(Vert_Lambdas[1].GetBlock()));

				Gammas[ia] = Contract_Horizontal_GamA_Inverse(Hori_GamA_Inverse_net, 
					{Gammas[ia], LamB_inv, LamUp_inv, LamDown_inv});
				Gammas[ib] = Contract_Horizontal_GamB_Inverse(Hori_GamB_Inverse_net, 
					{Gammas[ib], LamB_inv, LamUp_inv, LamDown_inv});

			} 
			else{
				int ia = i % 2;
				int ib = abs(ia - 1);

				Theta = Contract_Vertical_Theta(Vertical_Theta_net, {Gammas[ia], Gammas[ib], 
					Vert_Lambdas[ia], Vert_Lambdas[ib], U_op, Hori_Lambdas[0], Hori_Lambdas[1]});
				uni10::Svd(Theta.GetBlock(), svd[0], svd[1], svd[2], uni10::INPLACE);

				uni10::Resize(svd[0], svd[0].row(), chi, uni10::INPLACE);
				uni10::Resize(svd[1], chi, chi, uni10::INPLACE);
				uni10::Resize(svd[2], chi, svd[2].col(), uni10::INPLACE);
				uni10::Transpose(svd[2], uni10::INPLACE);
				svd[1] *= (1 / uni10::Norm(svd[1]));

				Gammas[ia].Assign({Theta.bond()[0], Theta.bond()[1], Theta.bond()[2], 
					uni10::Bond(uni10::BD_IN, chi), Theta.bond()[3].dummy_change(uni10::BD_OUT)});
				Vert_Lambdas[ia].Assign({uni10::Bond(uni10::BD_IN, chi), uni10::Bond(uni10::BD_OUT, chi)});
				Gammas[ib].Assign({Theta.bond()[4].dummy_change(uni10::BD_IN), uni10::Bond(uni10::BD_IN, chi), 
					Theta.bond()[5].dummy_change(uni10::BD_IN), Theta.bond()[6].dummy_change(uni10::BD_IN), Theta.bond()[7]});

				uni10::Permute(Gammas[ia], {0, 1, 2, 4, 3}, 4, uni10::INPLACE);
				uni10::Permute(Gammas[ib], {0, 2, 3, 4, 1}, 4, uni10::INPLACE);

				Gammas[ia].PutBlock(svd[0]);
				Vert_Lambdas[ia].PutBlock(svd[1]);
				Gammas[ib].PutBlock(svd[2]);

				uni10::Permute(Gammas[ia], {0, 1, 2, 3, 4}, 2, uni10::INPLACE);
				uni10::Permute(Gammas[ib], {0, 1, 2, 3, 4}, 2, uni10::INPLACE);

				LamB_inv.Assign(Vert_Lambdas[ib].bond());
				LamLeft_inv.Assign(Hori_Lambdas[0].bond());
				LamRight_inv.Assign(Hori_Lambdas[1].bond());

				LamB_inv.PutBlock(uni10::Inverse(Vert_Lambdas[ib].GetBlock()));
				LamLeft_inv.PutBlock(uni10::Inverse(Hori_Lambdas[0].GetBlock()));
				LamRight_inv.PutBlock(uni10::Inverse(Hori_Lambdas[1].GetBlock()));

				Gammas[ia] = Contract_Vertical_GamA_Inverse(Vert_GamA_Inverse_net, 
					{Gammas[ia], LamB_inv, LamLeft_inv, LamRight_inv});
				Gammas[ib] = Contract_Vertical_GamB_Inverse(Vert_GamB_Inverse_net, 
					{Gammas[ib], LamB_inv, LamLeft_inv, LamRight_inv});
			}
		}
	}
}


uni10::UniTensor<uni10_double64> Contract_Horizontal_LGLGL(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamA", tens[0]);
	net.PutTensor("GamB", tens[1]);
	net.PutTensor("LamA", tens[2]);
	net.PutTensor("LamB", tens[3]);
	net.PutTensor("LamUp", tens[4]);
	net.PutTensor("LamDown", tens[5]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);
	return Tout;
}

uni10::UniTensor<uni10_double64> Contract_Horizontal_Theta(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamA", tens[0]);
	net.PutTensor("GamB", tens[1]);
	net.PutTensor("LamA", tens[2]);
	net.PutTensor("LamB", tens[3]);
	net.PutTensor("U", tens[4]);
	net.PutTensor("LamUp", tens[5]);
	net.PutTensor("LamDown", tens[6]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);
	return Tout;
}

uni10::UniTensor<uni10_double64> Contract_Vertical_LGLGL(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamA", tens[0]);
	net.PutTensor("GamB", tens[1]);
	net.PutTensor("LamA", tens[2]);
	net.PutTensor("LamB", tens[3]);
	net.PutTensor("LamLeft", tens[4]);
	net.PutTensor("LamRight", tens[5]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);
	return Tout;	
}

uni10::UniTensor<uni10_double64> Contract_Vertical_Theta(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamA", tens[0]);
	net.PutTensor("GamB", tens[1]);
	net.PutTensor("LamA", tens[2]);
	net.PutTensor("LamB", tens[3]);
	net.PutTensor("U", tens[4]);
	net.PutTensor("LamLeft", tens[5]);
	net.PutTensor("LamRight", tens[6]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);
	return Tout;
}

uni10::UniTensor<uni10_double64> Contract_Horizontal_GamA_Inverse(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamA", tens[0]);
	net.PutTensor("LamB", tens[1]);
	net.PutTensor("LamUp", tens[2]);
	net.PutTensor("LamDown", tens[3]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);

	return Tout;
}

uni10::UniTensor<uni10_double64> Contract_Horizontal_GamB_Inverse(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamB", tens[0]);
	net.PutTensor("LamB", tens[1]);
	net.PutTensor("LamUp", tens[2]);
	net.PutTensor("LamDown", tens[3]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);

	return Tout;	
}

uni10::UniTensor<uni10_double64> Contract_Vertical_GamA_Inverse(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamA", tens[0]);
	net.PutTensor("LamB", tens[1]);
	net.PutTensor("LamLeft", tens[2]);
	net.PutTensor("LamRight", tens[3]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);

	return Tout;
}

uni10::UniTensor<uni10_double64> Contract_Vertical_GamB_Inverse(
	uni10::Network net, vector<uni10::UniTensor<uni10_double64> > tens){

	net.PutTensor("GamB", tens[0]);
	net.PutTensor("LamB", tens[1]);
	net.PutTensor("LamLeft", tens[2]);
	net.PutTensor("LamRight", tens[3]);

	uni10::UniTensor<uni10_double64> Tout;
	net.Launch(Tout);

	return Tout;

}